package com.horzelam.example.akka.streams.utils;

import org.influxdb.dto.Point;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

public class MsgUtils {
    static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("hh:mm:ss.SSS");

    public static String generateInputMsg(final Integer elem) {
        InfluxUtils.influxDB.write(
                Point.measurement("produce-1").time(System.currentTimeMillis(), TimeUnit.MILLISECONDS).addField("msgs", 1L).build());
        return currDate() + " - " + elem;
    }

    public static void traceMsgReceived(String name) {
        InfluxUtils.influxDB.write(
                Point.measurement(name).time(System.currentTimeMillis(), TimeUnit.MILLISECONDS).addField("msgs", 1L).build());
    }

    public static String currDate() {
        return formatter.format(LocalTime.now());
    }
}
