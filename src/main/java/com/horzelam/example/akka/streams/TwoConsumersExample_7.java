package com.horzelam.example.akka.streams;

import akka.NotUsed;
import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.dispatch.BoundedMessageQueueSemantics;
import akka.dispatch.RequiresMessageQueue;
import akka.stream.ActorMaterializer;
import akka.stream.ActorMaterializerSettings;
import akka.stream.Graph;
import akka.stream.Materializer;
import akka.stream.SinkShape;
import akka.stream.UniformFanOutShape;
import akka.stream.javadsl.Balance;
import akka.stream.javadsl.GraphDSL;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import com.horzelam.example.akka.streams.utils.InfluxUtils;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import java.time.Duration;

import static com.horzelam.example.akka.streams.utils.MsgUtils.generateInputMsg;
import static com.horzelam.example.akka.streams.utils.MsgUtils.traceMsgReceived;

/**
 * How 2 consumers are
 */
public class TwoConsumersExample_7 {

    static final Config conf = ConfigFactory.load("application.conf");

    static final ActorSystem system = ActorSystem.create("system1", conf);

    static final Materializer materializer =
            ActorMaterializer.create(ActorMaterializerSettings.create(system).withInputBuffer(1, 1), system);

    /**
     * Throttles one Source with other Tick Source (every 200 ms)
     */
    public static void main(String[] args) {

        // Throttled Source
        final Source<String, NotUsed> throttledSource = throttledSource(6000, 30);

        // Sinks: Fast and Slow Actors:
        final ActorRef slowDownActorSink = system.actorOf(SlowDownActor.props("slow-consumer", 0, 1, 100)
                //.withMailbox("small-mailbox")
                , "slowDownActor-actor");
        final ActorRef fastActorSink = system.actorOf(SlowDownActor.props("fast-consumer", 0, 0, 200000000)
                //.withMailbox("small-mailbox")
                , "fastActor-actor");
        final Graph<SinkShape<String>, NotUsed> sinkSlow = Sink.actorRef(slowDownActorSink, "end-of-stream");
        final Graph<SinkShape<String>, NotUsed> sinkFast = Sink.actorRef(fastActorSink, "end-of-stream");

        final Graph<SinkShape<String>, NotUsed> sinkGraph = GraphDSL.create(builder -> {
            final UniformFanOutShape<String, String> balance = builder.add(Balance.create(2));
            builder.from(balance.out(0)).toInlet(builder.add(sinkSlow).in());
            builder.from(balance.out(1)).toInlet(builder.add(sinkFast).in());

            // Exposing exactly one input port
            return new SinkShape<String>(balance.in());
        });

        throttledSource.to(sinkGraph).run(materializer);

    }

    private static Source<String, NotUsed> throttledSource(final int totalMessages, final long delayMs) {
        final Source<String, NotUsed> source = Source.range(1, totalMessages).map(elem -> generateInputMsg(elem));
        // We directly use throttle transformation:
        return source.throttle(1, Duration.ofMillis(delayMs));
    }

    static final class SlowDownActor extends AbstractActor implements RequiresMessageQueue<BoundedMessageQueueSemantics> {
        private int slowDownAfterMsgs;
        private final String name;
        private long delay;
        private long delayIncrease;
        private int msgCounter = 0;

        public SlowDownActor(final String name, final long initialDelay, final long delayIncrease, final int slowDownAfterMsgs) {
            this.name = name;
            this.delay = initialDelay;
            this.delayIncrease = delayIncrease;
            this.slowDownAfterMsgs = slowDownAfterMsgs;
        }

        public static Props props(final String name, final long initialDelay, long delayIncrease, final int slowDownAfterMsgs) {
            return Props.create(SlowDownActor.class, () -> new SlowDownActor(name, initialDelay, delayIncrease, slowDownAfterMsgs));
        }

        @Override
        public Receive createReceive() {
            return receiveBuilder()
                    //END OF STREAM:
                    .matchEquals("end-of-stream", s -> {
                        System.out.println("Finishing .. ");
                        InfluxUtils.influxDB.close();
                        this.getContext().system().terminate();
                    })
                    // STREAM MSG:
                    .matchAny(msg -> {
                        msgCounter++;
                        traceMsgReceived(name);
                        if(msgCounter >= slowDownAfterMsgs) {
                            delay += delayIncrease;
                            Thread.sleep(delay);
                        }
                        //                        System.out.println(currDate() + name+ " Msg : " + msg);
                    }).build();
        }

    }
}
