package com.horzelam.example.akka.streams;

import akka.NotUsed;
import akka.actor.ActorSystem;
import akka.stream.ActorMaterializer;
import akka.stream.Materializer;
import akka.stream.javadsl.Source;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

public class SimplestExample_1 {

    /**
     * Just prints the stream values 1..100 to stdout
     */
    public static void main(String[] args) {

        // We need Materializer (and actor system) to run the streams:
        // Materializer - factory for stream execution engines
        final ActorSystem system = ActorSystem.create("system1");
        final Materializer materializer = ActorMaterializer.create(system);

        // At the begining we have some simple , not materialized source:
        final Source<String, NotUsed> source = Source.range(1, 20)
                .map(elem -> currDate() + " - " + elem);

        // we have to run it
        source.runForeach(i -> System.out.println(i), materializer)
                .thenRun(() -> system.terminate());

        // Example with FileIO Sink:
        //        source. map(text-> ByteString.fromString(text + "\n")).
        //                runWith(FileIO.toPath(Paths.get("results.txt")), materializer);
    }

    static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("hh:mm:ss");

    private static String currDate() {
        return formatter.format(LocalTime.now());
    }

}
