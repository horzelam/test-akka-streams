package com.horzelam.example.akka.streams.utils;

import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;

public class InfluxUtils {
    public static final InfluxDB influxDB = createInfluxClient() ;

    private static InfluxDB createInfluxClient() {
        final InfluxDB influx = InfluxDBFactory.connect("http://localhost:8086", "admin", "admin");
        influx.createDatabase("test-akka-streams");
        influx.setDatabase("test-akka-streams");
        return influx;
    }
}
