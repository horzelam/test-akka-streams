package com.horzelam.example.akka.streams;

import akka.NotUsed;
import akka.actor.AbstractActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.dispatch.BoundedMessageQueueSemantics;
import akka.dispatch.RequiresMessageQueue;
import akka.stream.ActorMaterializer;
import akka.stream.ActorMaterializerSettings;
import akka.stream.Materializer;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

import static com.horzelam.example.akka.streams.utils.MsgUtils.generateInputMsg;
import static com.horzelam.example.akka.streams.utils.MsgUtils.traceMsgReceived;

public class SlowDownActorExample_4 {

    static final Config conf = ConfigFactory.load("application.conf");

    static final ActorSystem system = ActorSystem.create("system1", conf);

    static final Materializer materializer =
            ActorMaterializer.create(ActorMaterializerSettings.create(system).withInputBuffer(1, 1), system);

    /**
     * Throttles one Source with other Tick Source (every 200 ms)
     */
    public static void main(String[] args) {

        // Throttled Source
        final Source<String, NotUsed> throttledSource = throttledSource(2000, 10);

        // Sink: Slow-down actor
        final ActorRef slowDownActorSink = system.actorOf(SlowDownActor.props(10, 1)
                //.withMailbox("small-mailbox")
                , "slowDownActor-actor");

        // Run it:
        throttledSource.to(Sink.actorRef(slowDownActorSink, "end-of-stream")).run(materializer);

        // Instead of terminating it here - msg: "end-of-stream" is sent at the end of the strream
        // and Actor decides what to do

    }

    private static Source<String, NotUsed> throttledSource(final int totalMessages, final long delayMs) {
        final Source<String, NotUsed> source = Source.range(1, totalMessages).map(elem -> generateInputMsg(elem));
        // We directly use throttle transformation:
        return source.throttle(1, Duration.ofMillis(delayMs));
    }

    static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("hh:mm:ss.SSS");

    private static String currDate() {
        return formatter.format(LocalTime.now());
    }

    static final class SlowDownActor extends AbstractActor implements RequiresMessageQueue<BoundedMessageQueueSemantics> {
        private long delayIncrease;
        private long delay;

        public SlowDownActor(final long initialDelay, final long delayIncrease) {
            this.delayIncrease = delayIncrease;
            this.delay = initialDelay;
        }

        public static Props props(final long initialDelay, long delayIncrease) {
            return Props.create(SlowDownActor.class, () -> new SlowDownActor(initialDelay, delayIncrease));
        }

        @Override
        public Receive createReceive() {
            return receiveBuilder()
                    //END OF STREAM:
                    .matchEquals("end-of-stream", s -> {
                        System.out.println("Finishing .. ");
                        this.getContext().system().terminate();
                    })
                    // STREAM MSG:
                    .matchAny(msg -> {
                        traceMsgReceived("consume-a-1");
                        delay += delayIncrease;

                        Thread.sleep(delay);
                        //System.out.println(currDate() + " Msg received in slow-down actor: " + msg  + " (delay: "+delay+")");
                    }).build();
        }
    }
}
