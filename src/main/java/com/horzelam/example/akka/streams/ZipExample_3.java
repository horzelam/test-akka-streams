package com.horzelam.example.akka.streams;

import akka.Done;
import akka.NotUsed;
import akka.actor.ActorSystem;
import akka.actor.Cancellable;
import akka.stream.ActorMaterializer;
import akka.stream.Materializer;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;

import java.time.Duration;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.CompletionStage;

public class ZipExample_3 {

    static final ActorSystem system = ActorSystem.create("system1");
    static final Materializer materializer = ActorMaterializer.create(system);

    /**
     * Throttles one Source with other Tick Source (every 200 ms)
     */
    public static void main(String[] args) {

        // Source + Tick Source (every 200 ms)
        final Source<String, NotUsed> source = Source.range(1, 20).map(elem -> currDate() + " - " + elem);
        final Source<Integer, Cancellable> tickSource = Source.tick(Duration.ofMillis(0), Duration.ofMillis(200), 0);

        // Combine source with Tick Source to control the flow:
        final Source<String, NotUsed> throttled = source.zip(tickSource).map(val -> val.first());

        // Sink: Printing elements
        final Sink<String, CompletionStage<Done>> printingSink = Sink.foreach(System.out::println);

        // materialize the flow, getting the Sinks materialized value
        final CompletionStage<Done> sum = throttled.runWith(printingSink, materializer);

        sum.thenRun(() -> system.terminate());
    }

    static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("hh:mm:ss.SSS");

    private static String currDate() {
        return formatter.format(LocalTime.now());
    }

}
