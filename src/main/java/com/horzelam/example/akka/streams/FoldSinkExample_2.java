package com.horzelam.example.akka.streams;

import akka.NotUsed;
import akka.actor.ActorSystem;
import akka.stream.ActorMaterializer;
import akka.stream.Materializer;
import akka.stream.javadsl.Keep;
import akka.stream.javadsl.RunnableGraph;
import akka.stream.javadsl.Sink;
import akka.stream.javadsl.Source;

import java.util.concurrent.CompletionStage;

public class FoldSinkExample_2 {
    // We need Materializer (and actor system) to run the streams:
    // Materializer - factory for stream execution engines
    static final ActorSystem system = ActorSystem.create("system1");
    static final Materializer materializer = ActorMaterializer.create(system);

    /**
     * Presents result of SOURCE->FOLD SINK - Folding the stream values
     */
    public static void main(String[] args) {

        // Source:
        final Source<Integer, NotUsed> source = Source.range(1, 10);

        // Fold Sink:
        final Sink<Integer, CompletionStage<Integer>> sink = Sink.<Integer, Integer>fold(0, (aggr, next) -> aggr + next);

        // connect the Source to the Sink, obtaining a RunnableFlow
        final RunnableGraph<CompletionStage<Integer>> runnable = source.toMat(sink, Keep.right());

        // run - materialize the Runnable Graph
        final CompletionStage<Integer> sum = runnable.run(materializer);

        // display the result :
        sum.thenAccept(result -> System.out.println("Sum is : " + result.toString())).thenRun(() -> system.terminate());
    }

}
