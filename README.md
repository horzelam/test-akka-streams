# Goal
Goal is to try Akka Streams with focus on backpressure, transformations.


![](docs/goal.png)


# Why using Akka Streams
To be able to signal **backpressure** and handle **temporary bursts**   
because it is critical to build resilient systems.

## Theory - Reactive streams
Provide stream processing tools which are **asynchronous** and support **non-blocking back-pressure**.

The interfaces available in JDK9’s java.util.concurrent.Flow, 
are 1:1 semantically equivalent to their respective Reactive Streams counterparts.

### How it started
* 2013 - RxJava team exploring addition of **backpressure** to RxJava
* 2014 -> team of engineers from different vendors started to talk to solve the problems
  * Viktor Kland (Typesafe) 
  * Roland Kuhn (Akka in Typesafe) 
  * Ben Christensen (RxJava and Netflix)
  * Stephane Maldini (Pivotal’s Project Reactor ) 
  * George Campbell 

## Poor Backpressure

**"Push-based"**
* consumer buffer + dropping (optimized for fast consumer)

**"Pull-based"**
* consumer asks for data (optimized for fast producer)  


## Reactive streams <-> Akka Streams

**According to docs**:  
The relationship between these two is that the **Akka Streams API** is geared towards end-users 
while the **Akka Streams** implementation **uses the Reactive Streams interfaces 
internally** to pass data between the different processing stages.   

For this reason you will not find any resemblance   
between the Reactive Streams interfaces and the Akka Streams API.   

This is in line with the expectations of the Reactive Streams project,   
whose primary purpose is to define interfaces such that different streaming    
implementation can interoperate; it is not the purpose of Reactive Streams to describe an end-user API.

![](docs/core-elements.png)

Important to notice - flow of : data + demand

Dynamic switch between the :
**Push** - when consumer is faster
**Pull** - when producer is faster

Another aspects:
* merging demand from 2 or more consumers
* backpressure propagation



## AKKA streams - theory

Akka (and Scala) having perfect DSL to build flows of data.

DSL elements: 
* Source - akka-streams term for a publisher; A processing stage with exactly one output; emits the data elements 
* Flow - performs transformation on elements produced by Source; A processing stage which has exactly one input and output,
* Sink - term for subscriber, consumes elements transformed in Flow; A processing stage with exactly one input

Akka Streams separate **what** from the **how**


A transformation pipeline executes asynchronously. 
**FlowMaterializer** turns this flow declarations into running Actors 
(you might be unaware of that Actor usage).

Akka stremns uses **bounded buffer space** - which is different from the **actor model**.
**Actor model** has usually unbounded or a bounded but **dropping mailbox**.
Akka Stream processing entities have bounded “mailboxes” that **do not drop**.
 
## AKKA streams - Transformations

Deterministic
* map 
* filter
* collect
* grouped
* drop
* take

Time-based:
* takeWithin
* groupedWithin

Rate-Detached
* expand 
* buffer

Asynchronous
* mapAsync
* mapAsyncUnordered
...

Nonlinear
* Fan-In : merge, concat, zip
* Fan-Out: broadcast, route, balance, unzip

# Akka Streams - integrations
Easy integration with:
* Kafka
* Http
* Files
* Actors
* Databases
* Websockets

# Goal of the task

Check how rate of a publisher and subscriber are nicely aligned when subscriber(s) becoming slow.  
The examples will use different type of Buffer (with backpressure/drop) and Balancer. 


## Install of influx db

Based on : http://www.andremiller.net/content/grafana-and-influxdb-quickstart-on-ubuntu

###  create a file called “/etc/apt/sources.list.d/influxdb.list”
```
curl -sL https://repos.influxdata.com/influxdb.key | sudo apt-key add -
source /etc/lsb-release
echo "deb https://repos.influxdata.com/${DISTRIB_ID,,} ${DISTRIB_CODENAME} stable" | sudo tee /etc/apt/sources.list.d/influxdb.list
```
### install it
```sudo apt-get update && sudo apt-get install influxdb```

### start it
```sudo service influxdb start```

### go into:
```influx```

### create db
```CREATE DATABASE statsdemo```

### show
```SHOW DATABASES```

### use it
```
USE statsdemo
INSERT cpu,host=serverA value=0.64
SELECT * from cpu
```

### send test data 
```
val=$(cat /proc/loadavg | cut -f1 -d" ") ; data="cpu,host=serverA value=$val"; curl -i -XPOST 'http://localhost:8086/write?db=statsdemo' --data-binary "$data";
```
or 
```
curl -i -XPOST 'http://localhost:8086/write?db=statsdemo' --data-binary 'cpu,host=serverA value=0.443'
```





## Install grafana
http://docs.grafana.org/installation/debian/
```
wget https://s3-us-west-2.amazonaws.com/grafana-releases/release/grafana_5.1.3_amd64.deb
sudo apt-get install -y adduser libfontconfig
sudo dpkg -i grafana_5.1.3_amd64.deb

sudo service grafana-server start
# This will start the grafana-server process as the grafana user, which was created during the package installation.
# The default HTTP port is 3000 and default user and group is admin.
# To configure the Grafana server to start at boot time:
# sudo update-rc.d grafana-server defaults
```

Log into: http://localhost:3000/?orgId=1
with admin/admin

Then configure db as influx db:
```
Name: statsdemo
Type: InfluxDB 0.9.x
Url: http://localhost:8086/
Database: statsdemo
User: admin
Password: admin
```


# Examples


## Producer + Slow-down consumer (no buffering) - backpressure on producer
![](docs/slow-down-consumer-no-buffer.png)
 


## Producer + Buffer + Slow-down consumer (slows down after some amount of msgs) 
**We can observe how backpressure kicks in after some time:**
![](docs/slow-down-afterXamount-consumer-buffer.png)



## Producer + Broadcast to Fast + Slow-down consumer
![](docs/example-broadcast.png)
**Slow consumer informing Producer also affects the fast consumer**  

## Producer + Balancer to 2 consumers: Fast & Slow-down
![](docs/example-balance.png)
Possible use case: automatically balance incoming jobs to available workers, then merges the results.

**Result - balancer dynamically adopts and changes the ratio between two sinks.
Producer is not affected, because fast consumer can compensate the slow one.**
![](docs/balance-on-slow-down-consumer-no-buffer.png)

# TODO: describe async boundry and how it's executed by actors
see: https://doc.akka.io/docs/akka/2.5/stream/stream-flows-and-basics.html#operator-fusion

## See other
* inspiration article - in Scala - http://www.smartjava.org/content/visualizing-back-pressure-and-reactive-streams-akka-streams-statsd-grafana-and-influxdb
* sub streams examples - https://doc.akka.io/docs/akka/current/stream/stream-substream.html
* Poor man’s back-pressure - https://manuel.bernhardt.io/2014/04/23/a-handful-akka-techniques/





# Presentation
See : https://docs.google.com/presentation/d/1nbWtIadp3HJCTanvxS6PihVsPdmFuirwrY-km8fL_pw/edit#slide=id.g1f87997393_0_1053